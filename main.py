from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import requests

import time
import sys
import json

class Parser:
    def __init__(self):
        self.WEBSITE = 'censored :)'
        self.WEBDRIVER_PATH = 'C:\Program Files (x86)\chromedriver.exe'
        self.TURBINES_LINKS = 'turbines_links.txt'
        self.TURBINES_INFO = 'turbines_info.txt'
        self.CAR_NAMES = 'car_names.txt'
        self.CARS_MODELS = 'cars_models.json'

    def update_turbine_links(self):
        driver = self.init_driver()

        driver.get(self.WEBSITE)

        cars = self.read_car_names()
        cars_models = self.read_cars_models()

        turbine_links = []
        for car in cars:
            self.choose_car_option(driver, car)
            for model in cars_models[car]:
                self.choose_model_option(driver, model)
                links_amount = self.find_links_amount(driver)
                turbine_links += self.find_all_links(links_amount, driver)
                with open(self.TURBINES_LINKS, 'w') as f:
                    f.write('\n'.join(turbine_links))
                #time.sleep(0.5)

        driver.quit()

    def init_driver(self):
        try:
            driver = webdriver.Chrome(self.WEBDRIVER_PATH)
            return driver
        except Exception as e:
            print('Couldn\'t get the driver -', str(e))
            sys.exit()

    def choose_car_option(self, driver, car):
        try:
            # find and click on car option in dropdown menu
            car = WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable(
                    (By.XPATH, f"//select[@id='ctl00_ContentPlaceHolder1_DropDownList1']/option[text()='{car}']"))
            )
            car.click()
        except Exception as e:
            print(str(e))
            driver.quit()

    def choose_model_option(self, driver, model):
        try:
            # find and click on model option in dropdown menu
            model = WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable(
                    (By.XPATH, f"//select[@id='ctl00_ContentPlaceHolder1_DropDownList2']/option[text()='{model}']"))
            )
            model.click()
        except Exception as e:
            print(str(e))
            driver.quit()

    def find_links_amount(self, driver):
        PAGE_REFRESH_TIME = 1.5
        time.sleep(PAGE_REFRESH_TIME)
        src = driver.page_source
        return src.count('ctl00_ContentPlaceHolder1_Grid1_ctl01_ctl')

    def find_all_links(self, links_amount, driver):
        element_id_cntr = 4
        links = []
        for _ in range(links_amount):
            id_number = str(element_id_cntr).zfill(2)
            links.append(driver.find_element_by_id(f"ctl00_ContentPlaceHolder1_Grid1_ctl01_ctl{id_number}_HyperLink1").get_attribute('href'))
            element_id_cntr += 2

        return links

    def get_turbine_info(self, link):
        driver = self.init_driver()
        driver.get(link)

        title = driver.find_element_by_class_name('h2n').text
        subtitle = driver.find_element_by_class_name('h3n').text
        card_title = driver.find_elements_by_class_name('boxSpacer')[-1].text

        card_text = driver.find_element_by_id('ctl00_ContentPlaceHolder1_Div2').text
        card_items = card_text.split('\n')[:5]

        model = card_items[0]
        engine = ' '.join(card_items[1].split(' ')[1:])
        capacity = ' '.join(card_items[2].split(' ')[1:])
        power = ' '.join(card_items[3].split(' ')[1:])
        year = ' '.join(card_items[4].split(' ')[2:])

        numbers = driver.find_elements_by_css_selector('ol')
        part_number = numbers[0].text.split('\n')
        manufacturer_number = numbers[1].text.split('\n')

        driver.close()

        return {'title': title,
                        'subtitle': subtitle,
                        'card_title': card_title,
                        'model': model,
                        'engine': engine,
                        'capacity': capacity,
                        'power': power,
                        'year': year,
                        'part_number': part_number,
                        'manufacturer_number': manufacturer_number}

    def get_turbine_links(self):
        links = []
        try:
            with open(self.TURBINES_LINKS, 'r') as f:
                while True:
                    line = f.readline()
                    if not line:
                        break
                    links.append(line.strip())
        except FileNotFoundError as e:
            print(str(e))
            print('Fix path to file with turbines links')
            sys.exit()
        return links

    def update_turbines_info(self, turbines_info):
        turbines_info_csv = []
        for turbine in turbines_info:
            line = ''
            for item in turbine:
                if type(turbine[item]) == list:
                    line += ' '.join(turbine[item])
                else:
                    line += turbine[item]
                line += '|'
            turbines_info_csv.append(line[:-1])
        with open(self.TURBINES_INFO, "w", encoding="utf-8") as f:
            f.write('\n'.join(turbines_info_csv))

    def get_car_names(self):
        driver = self.init_driver()
        driver.get(self.WEBSITE)

        car_names = []
        elements = driver.find_elements_by_css_selector('option')
        for el in elements:
            car_names.append(el.text)

        driver.quit()
        return car_names[1:]

    def update_car_names(self):
        car_names = self.get_car_names()
        with open(self.CAR_NAMES, 'w', encoding='utf-8') as f:
            f.write('\n'.join(car_names))

    def read_car_names(self):
        cars = []
        with open(self.CAR_NAMES, 'r', encoding='utf-8') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                cars.append(line.strip())
        return cars

    def get_cars_models(self):
        driver = self.init_driver()
        driver.get(self.WEBSITE)

        cars = []
        with open(self.CAR_NAMES, 'r') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                cars.append(line.strip())

        cars_models = {}
        for car in cars:
            self.choose_car_option(driver, car)

            time.sleep(1.5)  # wait models loading
            block = driver.find_element_by_name('ctl00$ContentPlaceHolder1$DropDownList2').find_elements_by_css_selector('option')

            cars_models.update({car: []})
            for model in block[1:]:
                cars_models[car].append(model.text)

        driver.quit()
        return cars_models

    def update_cars_models(self):
        cars_models = self.get_cars_models()
        with open(self.CARS_MODELS, 'w', encoding='utf-8') as f:
            json.dump(cars_models, f)

    def read_cars_models(self):
        with open(self.CARS_MODELS, 'r') as f:
            return json.load(f)

    def bs4_get_turbine_info(self, link):
        r = requests.get(link)
        soup = BeautifulSoup(r.text, features="html.parser")

        title = soup.find("h2", class_="h2n").text.strip().replace('\r', '').replace('\n', '').split('    ')
        title.sort(reverse=True)
        title = ' '.join(title[:2])
        subtitle = soup.find("h3", class_="h3n").text
        card_title = soup.find_all("div", class_="boxSpacer")[-1].text.strip()

        model = str(soup.find("div", id="ctl00_ContentPlaceHolder1_Div2")).split('<br/>')[0].split('>')[1].lstrip()
        engine = ' '.join(str(soup.find("div", id="ctl00_ContentPlaceHolder1_Div2")).split('<br/>')[1].split(' ')[1:])
        capacity = ' '.join(str(soup.find("div", id="ctl00_ContentPlaceHolder1_Div2")).split('<br/>')[2].split(' ')[1:])
        power = ' '.join(str(soup.find("div", id="ctl00_ContentPlaceHolder1_Div2")).split('<br/>')[3].split(' ')[1:])
        year = ' '.join(str(soup.find("div", id="ctl00_ContentPlaceHolder1_Div2")).split('<br/>')[4].split(' ')[2:])

        numbers = soup.find_all("ol")
        part_numbers_li = numbers[0].find_all("li")
        part_number = []
        for n in part_numbers_li:
            part_number.append(n.text)

        man_numbers_li = numbers[1].find_all("li")
        man_number = []
        for n in man_numbers_li:
            man_number.append(n.text)

        return {'title': title,
                        'subtitle': subtitle,
                        'card_title': card_title,
                        'model': model,
                        'engine': engine,
                        'capacity': capacity,
                        'power': power,
                        'year': year,
                        'part_number': part_number,
                        'manufacturer_number': man_number}


if __name__ == '__main__':
    parser = Parser()

    #parser.update_turbine_links()

    links = parser.get_turbine_links()

    turbines_info = []
    for link in links:
        turbines_info.append(parser.bs4_get_turbine_info(link))
        print(len(turbines_info), turbines_info[-1])
        #time.sleep(0.1)
    parser.update_turbines_info(turbines_info)


    #parser.update_cars_models()
